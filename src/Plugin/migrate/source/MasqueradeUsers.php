<?php

namespace Drupal\masquerade_nominate\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 *
 * @MigrateSource(
 *   id = "d7_masquerade_users",
 *   source_module = "masquerade_nominate"
 * )
 */
class MasqueradeUsers extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // NB Only overriden ctools objects are even stored in the database.
    return $this->select('masquerade_users', 'mu')
      ->fields('mu', ['uid_from', 'uid_to']);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'uid_from' => $this->t('The user'),
      'uid_to' => $this->t('The masquerader')
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'uid_from' => [
        'type' => 'integer',
      ],
      'uid_to' => [
        'type' => 'integer',
      ],
    ];
  }

}
// On rollback
//Column not found: 1054 Unknown column 'destid1' in 'order clause':
//SELECT map.sourceid1 AS sourceid1, map.sourceid2 AS sourceid2
//FROM
//{migrate_map_d7_masquerade_users} map
//ORDER BY destid1 ASC;