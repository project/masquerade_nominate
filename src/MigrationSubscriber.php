<?php

namespace Drupal\masquerade_nominate;

use Drupal\migrate\Event\MigrateImportEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @todo this has been left undone. Not sure what it is supposed to do.
 */ 
class MigrationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      'migrate.post_import' => [['migratePostImport']]
    ];
  }


  function migratePostImport(MigrateImportEvent $event) {
    $migration = $event->getMigration();
    $sourcePlugin = $migration->getSourcePlugin();
    if ($migration->id() == 'd7_user') {

    }

  }


}
