<?php

namespace Drupal\masquerade_nominate\Plugin\migrate\process;

use Drupal\user\Entity\User;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate\Attribute\MigrateProcess;

/**
 * Add a nominee to a user.
 * @deprecated
 */
#[MigrateProcess('masquerade_nominee')]
class MasqueradeNominee extends ProcessPluginBase {

  /**
   * From something like
   * [transaction:payer] paid [transaction:payee] [transaction:worth]. [transaction:links]
   * to
   * [xaction:payer] paid [xaction:payee] [xaction:worth] for [xaction:description].
   */
  public function transform($uid_to, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $user = User::load($uid_to);
    $nominees = $user->masquerade_nominees->getValue();
    $nominees[] = $row->getSourceProperty('uid_from');
    $user->masquerade_nominees->setValue($nominees);
    $user->save();
    return $nominees;
  }

}
