<?php

use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Link;

/**
 * Implements masquerade hook masquerade_access(),
 * whether the current user can masquerade as the target account.
 */
function masquerade_nominate_masquerade_access($currentUser, $target_account) {
  $allowed_as = masquerade_nominate_masquerade_as($currentUser);
  if (isset($allowed_as[$target_account->id()])) {
    return TRUE;
  }
}

/**
 * get the users which the given user can masquerade as.
 *
 * @returns AccountInterface[]
 */
function masquerade_nominate_masquerade_as($account = NULL) {
  if (is_null($account)) {
    $account = \Drupal::currentUser();
  }
  return \Drupal::entityQuery('user')
    ->accessCheck(TRUE)
    ->condition('masquerade_nominees', $account->id())
    ->execute();
}


/**
 * Implements hook_page_top().
 *
 * The only way I can see to interject in every page.
 */
function masquerade_nominate_page_top() {
  if (\Drupal::service('masquerade')->isMasquerading()){
    // this is a bit strange but its necessary to get the CSRF token right
    $renderable = [
      '#title' => t('Unmasquerade'),
      '#type' => 'link',
      '#url' => Url::fromRoute('masquerade.unmasquerade')
    ];
    $link = \Drupal::Service('renderer')->renderRoot($renderable);
    \Drupal::messenger()->addStatus(t('You are masquerading. @link', ['@link' => $link]));
  }
}

/**
 * Implements hook_menu_links_discovered_alter().
 *
 * Remove the link masquerade.unmasquerade from the user menu.
 * cd ../
 * @see masquerade.links.menu.yml
 */
function masquerade_nominate_menu_links_discovered_alter(&$links) {
  unset($links['masquerade.unmasquerade']);
}

/**
 *
 * @param type $form
 * @param type $form_state
 * @return type
 */
function masquerade_nominate_form_masquerade_block_form_alter(&$form, $form_state) {
  // If the user has any permissions to masquerade, don't change anything

  foreach (\Drupal::service('masquerade')->getPermissions() as $perm) {
    if (\Drupal::CurrentUser()->hasPermission($perm)) return;
  }
  unset($form['autocomplete']);

  $links = [];
  $form['#title'] = t('Use the site as…');
  $target_ids = masquerade_nominate_masquerade_as();
  foreach (User::loadMultiple($target_ids) as $account) {
    $links[] = Link::createFromRoute(
      $account->getDisplayName(),
     'entity.user.masquerade',
     ['user' => $account->id()]
    )->toString();
  }
  if (count($links)) {
    $form['targets'] = [
      '#theme' => 'item_list',
      '#items' => $links
    ];
  }
}

/**
 *
 * @param \Drupal\block\Entity\Block $block
 * @param type $operation
 * @param \Drupal\Core\Session\AccountInterface $account
 */
function masquerade_nominate_block_access($block, $operation, $account) {
  if ($block->getPluginId() == 'masquerade' and $operation == 'view') {
    return AccessResult::allowedif(!empty(masquerade_nominate_masquerade_as()))->cachePerUser();
  }
}

