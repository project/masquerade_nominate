<?php

namespace Drupal\masquerade_nominate\Plugin\migrate\destination;

use Drupal\user\Entity\User;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Attribute\MigrateDestination;

/**
 * Saves the masquerade users as the entity reference field on the user.
 */
#[MigrateDestination('masquerade_nominees')]
class MasqueradeNominees extends DestinationBase {

  protected $supportsRollback = TRUE;

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = array()) {
    $masquerader = $row->getDestinationProperty('masquerader');
    $masqueradee = $row->getDestinationProperty('masqueradee');
    $user = User::load($masqueradee);
    if (!$user) {
      throw new MigrateSkipRowException;
    }
    $val = $user->masquerade_nominees->getValue();
    $val[] = ['target_id' => $masquerader];
    $user->masquerade_nominees->setValue($val);
    $user->save();
    return [$masquerader, $masqueradee];
  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    $user = User::load($destination_identifier['masqueradee']);
    $val = $user->masquerade_nominees->getValue();
    foreach ($val as $delta => $user_ref) {
      if ($user_ref['target_id'] == $destination_identifier['masqueradee']) {
        unset($val[$delta]);
      }
    }
    $user->masquerade_nominees->setValue($val);
    $user->save();
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'masquerader' => 'The noiminated user',
      'masqueradee' => 'The user nominating another user'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'masquerader' => [
        'type' => 'integer'
      ],
      'masqueradee' => [
        'type' => 'integer'
      ]
    ];
  }

}
